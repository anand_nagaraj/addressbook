package com.anand.srccode.reader;

/**
 * @author anandnagaraj
 *
 */
public class ServiceReaderInjector implements ReaderInjector {
	
	@Override
	public ReaderInterface getConsumerReader(Object obj)
	{
		String book = (String)obj;
		BookReader reader = new BookReader(book);
		return reader;
	}

}
