package com.anand.srccode.reader;

/**
 * 
 * @author anandnagaraj
 *
 */
public interface ReaderInjector {

	public ReaderInterface getConsumerReader(Object obj);

}
