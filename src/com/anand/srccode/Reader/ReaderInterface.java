package com.anand.srccode.reader;

import java.io.IOException;


public interface ReaderInterface
{
	/**
	 * Reading a book from the specific path (in csv format).
	 * Store them in SortedMap.
	 * 
	 * @throws IOException
	 */
	public void read() throws IOException;
	
	/**
	 * Pass the keySet for further processing
	 */
	public Object keySet();

	/**
	 * Display the entries in the file in sorted manner.
	 */
	public void displayRecords();
	
	/**
	 * Insert the data to DataStructure
	 */
	public void insert(Object[] attributes);
	
	/**
	 * Get the records back to application
	 * @return
	 */
	public Object getRecords();
	
}
