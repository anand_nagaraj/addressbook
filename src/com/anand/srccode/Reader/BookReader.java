package com.anand.srccode.reader;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * Class: BookReader class which reads the file from the specific path,
 * reads them in memory and tries to operate "Set" operations (Union in particular).
 * 
 * @author anandnagaraj
 *
 */
public class BookReader implements ReaderInterface
{
	private String m_name;
	private SortedMap<String, Integer> m_sm;
	private String leftAlignFormat = "| %-3d | %-20s | %-12d |%n";
	
	public BookReader(String name)
	{
		this.m_name = name;		
		this.m_sm = new TreeMap<String, Integer>();
	}
	
	private void prefixFormattedDisplay()
	{
		System.out.format("|----------- Phone book Entries ------------|%n%n");
		System.out.format("+-----+----------------------+--------------+%n");
		System.out.format("| Num |      Name            |   Phone      |%n");
		System.out.format("+-----+----------------------+--------------+%n");
	}
	
	private void suffixFormattedDisplay()
	{
		System.out.format("+-----+----------------------+--------------+%n");
	}
	
	
	
	/**
	 * Get the records back to application
	 * @return
	 */
	@Override
	public Object getRecords() {
		return this.m_sm;
	}
	
	/**
	 * Insert the data to DataStructure
	 */
	@Override
	public void insert(Object[] attributes)
	{	
        // Validate parseInt()
		m_sm.put((String)attributes[0], Integer.parseInt((String)attributes[1]));
	}
	
	
	/**
	 * Pass the keySet for further processing
	 */
	@Override
	public Object keySet()
	{
		return m_sm.keySet();
	}
	
	
	/**
	 * Reading a book from the specific path (in csv format).
	 * Store them in SortedMap.
	 * 
	 * @throws IOException
	 */
	@Override
	public void read() throws IOException
	{	
		Path pathToFile = Paths.get(this.m_name);
		
		try (BufferedReader br = Files.newBufferedReader(pathToFile, StandardCharsets.US_ASCII)) {

            // read the first line from the text file
            String line = br.readLine();

            // loop until all lines are read
            while (line != null) {

                // split the record using , delimiter
                String[] attributes = line.split(",");
                
                // Adding these at
                insert(attributes);

                line = br.readLine();
            }
        } catch (IOException ioe) {
        	throw ioe;
        }
				
	}
	
	/**
	 * Display the entries in the file in sorted manner.
	 */
	@Override
	public void displayRecords()
	{
		prefixFormattedDisplay();
		int count = 1;
		for (String name : m_sm.keySet()) {
			Integer phone = (Integer)m_sm.get(name);
			System.out.printf(leftAlignFormat, count++, name, phone);		    
		}
		suffixFormattedDisplay();
	}
}