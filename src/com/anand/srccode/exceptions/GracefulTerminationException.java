package com.anand.srccode.exceptions;

public class GracefulTerminationException extends Exception
{
	/**
	 * A user defined exception class to handle when necessary 
	 * parameters are not passed to the program.
	 */
	private static final long serialVersionUID = 101L;

	public GracefulTerminationException(String str) {
		super(str);
	}
	
	public GracefulTerminationException() {
		super();
	}
}
