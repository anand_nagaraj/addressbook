package com.anand.srccode.exceptions;

/**
 * Class UsageException, A user defined class inherited from Exception class.
 * 
 * @author anandnagaraj
 *
 */
public class UsageException extends Exception
{
	/**
	 * A user defined exception class to handle when necessary 
	 * parameters are not passed to the program.
	 */
	private static final long serialVersionUID = 100L;

	public UsageException(String str) {
		super(str);
	}
	
	public UsageException() {
		super();
	}
}