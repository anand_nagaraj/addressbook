package com.anand.srccode;

import static org.junit.Assert.*;
import java.util.SortedSet;
import java.util.TreeSet;

import org.junit.Before;
import org.junit.Test;

import com.anand.srccode.operation.OperationInterface;
import com.anand.srccode.operation.OperationBookReader;


/**
 * Class OperatorTest,  This is to perform a unit-test methodology
 *  on what union can be performed on OperationBookReader.
 *  
 * @author anandnagaraj
 *
 */
public class OperatorTest
{	
	OperationInterface oper;
	SortedSet<String> s1;
	SortedSet<String> s2;
	
	@Before
	public void setUp() throws Exception {
		s1 = new TreeSet<String>();
		s2 = new TreeSet<String>();
		
		s1.add("Bob");
		s1.add("Mary");
		s1.add("Jane");
		
		s2.add("Mary");
		s2.add("John");
		s2.add("Jane");
		
		oper = new OperationBookReader();
	}

	/**
	 * Test for unique value same
	 */
	@Test
	public void testUniqueKeysSame()
	{
		SortedSet<String> s3;
		s3 = new TreeSet<String>();
		s3.add("Bob");
		s3.add("John");
		
		@SuppressWarnings("unchecked")
		SortedSet<String> unionKey = (SortedSet<String>) oper.gatherUniqueKey(s1, s2);
		assertTrue(unionKey.equals(s3));
		
	}
	
	/**
	 * Test for unique values are different
	 */
	@Test
	public void testUniqueKeysDifferent()
	{
		SortedSet<String> s4;
		s4 = new TreeSet<String>();
		s4.add("Mary");
		s4.add("John");
		
		@SuppressWarnings("unchecked")
		SortedSet<String> unionKey = (SortedSet<String>) oper.gatherUniqueKey(s1, s2);
		
		assertFalse(unionKey.equals(s4));
	}	

}
