package com.anand.srccode;

import java.io.IOException;
import java.nio.file.NoSuchFileException;

import com.anand.srccode.application.AddressBookApplication;
import com.anand.srccode.exceptions.GracefulTerminationException;
import com.anand.srccode.exceptions.UsageException;


/**
 * Class AddressBookMain, is the beginning of the program.
 * 
 * 
 * @author anandnagaraj
 *
 */
public class AddressBookMain
{	
	private static final int MAX_ARGUMENTS = 2;

	
	/**
	 * This is the start of the main function for this project,
	 * It tries to read the files (book1) and (book2) as specified in the argument.
	 * Each book should follow the set of guidelines
	 * name,phone
	 * As phone treated as a numerical number, its advised to have only numbers in it.
	 * name could be alpha-numerical
	 * phone is strictly a number.
	 * 
	 * Displaying the phone list in a formatted, aligned manner.
	 * 
	 * @param args
	 */
	public static void main(String[] args)
	{	
		try {
			if (args.length != MAX_ARGUMENTS) {
				System.out.println("Correct Usage for parameters : "
						+ "java com.anand.srccode.AddressBookMain (abs|rel)book1_path (abs|rel)book2_path");

				throw new UsageException("Incorrect Usage");
			}

			// Initialize the application and run
			AddressBookApplication app = new AddressBookApplication(args[0], args[1]);			
			app.run();

		} catch (UsageException ue) {
			ue.printStackTrace();
			System.exit(1); // Forceful exit from the program. 
		} catch (NoSuchFileException nfe) {
			nfe.printStackTrace();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		} catch (GracefulTerminationException gfte) {
			System.out.println("Gracefully terminated.... Bye");
		}
	}

}
