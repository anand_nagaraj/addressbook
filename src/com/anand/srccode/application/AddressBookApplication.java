package com.anand.srccode.application;

import java.io.IOException;
import java.nio.file.NoSuchFileException;
import java.util.Scanner;

import com.anand.srccode.reader.ReaderInterface;

import com.anand.srccode.writer.ServiceWriterInjector;
import com.anand.srccode.writer.WriterInjector;
import com.anand.srccode.writer.WriterInterface;

import com.anand.srccode.reader.ServiceReaderInjector;
import com.anand.srccode.reader.ReaderInjector;
import com.anand.srccode.operation.OperationBookReader;
import com.anand.srccode.operation.OperationInterface;
import com.anand.srccode.application.OptionChoice;
import com.anand.srccode.exceptions.GracefulTerminationException;

/**
 * Class AddressBookApplication, is the start of application where it is a consumer.
 * 
 * @author anandnagaraj
 *
 */
public class AddressBookApplication
{
	private String book1;
	private String book2;
	
	private ReaderInterface reader1;
	private ReaderInterface reader2;
	private WriterInterface writer1;
	private WriterInterface writer2;

	
	public AddressBookApplication(String b1, String b2)
	{
		this.book1 = b1;
		this.book2 = b2;

		// Initialize injectors
		ReaderInjector injectReader = new ServiceReaderInjector();
		WriterInjector injectWriter = new ServiceWriterInjector();

		// Initialize the book readers
		this.reader1 = injectReader.getConsumerReader(b1); // new BookReader(b1);
		this.reader2 = injectReader.getConsumerReader(b2); // new BookReader(b2);
		
		// Initialize the book writers
		this.writer1 = injectWriter.getConsumerWriter(b1); // new BookWriter(b1);
		this.writer2 = injectWriter.getConsumerWriter(b2); // new BookWriter(b2);
		
	}
	
	/**
	 * Begin to read the file, report any errors there after while reading
	 * 
	 * @throws NoSuchFileException
	 * @throws IOException
	 */
	private void startReadingFromBook() throws NoSuchFileException, IOException
	{
		try {
			this.reader1.read();
			this.reader2.read();
			
		} catch (NoSuchFileException nfe) {
			throw nfe;
		} catch (IOException ioe) {
			throw ioe;
		}
	}
	
	/**
	 * Used only to display the contents of the files Ordered by key (name)
	 */
	private void displayAllContactsFromBook(ReaderInterface reader, String bName)
	{
		System.out.println("\nDisplaying sorted records in \"" + bName + "\"");
		reader.displayRecords();

	}

	/**
	 * Display Unique Keys from each book and display them in ordered manner (using name as key)
	 */
	private void displayUniqueKeysFromEachBook()
	{
		// Perform some operation on both the books.		
		OperationInterface op = new OperationBookReader();

		System.out.println("\nDisplaying Unique key sets from both Books....");
		op.displayUniqueKeys(reader1.keySet(), reader2.keySet());
	}

	
	/**
	 * Begin running the main application.
	 * 
	 * @throws GracefulTerminationException
	 * @throws IOException
	 */
	public void run() throws GracefulTerminationException, IOException
	{
		startReadingFromBook();
		
		// use Scanner to get input from the user
		Scanner inKeyboard = new Scanner(System.in);
		
		Options opt = new Options(this.book1, this.book2);
		
		Integer choice;
		OptionChoice optionChoice;
		
		do {
			// Provide necessary options available for user to choose
			opt.displayTheOptions();
			
			try {
				choice = Integer.parseInt(inKeyboard.next());
				optionChoice = OptionChoice.valueOf(choice);
			} catch (NumberFormatException ne) {
				System.out.println("Incorrect input given: Not an integer. Please try again.\n\n\n");
				continue;
			}
			
			switch (optionChoice) {
			case ONE:
				displayAllContactsFromBook(reader1, this.book1);
				break;
			case TWO:
				displayAllContactsFromBook(reader2, this.book2);
				break;
			case THREE:
				displayAllContactsFromBook(reader1, this.book1);
				displayAllContactsFromBook(reader2, this.book1);
				break;
			case FOUR:
				displayUniqueKeysFromEachBook();
				break;
			case FIVE:
				System.out.println("Add new entry in \"name,phone\" format without quotes");
				String newInputStr1 = inKeyboard.next();
				String[] attributes1 = newInputStr1.split(",");
				reader1.insert(attributes1);
				break;
			case SIX:
				System.out.println("Add new entry in \"name,phone\" format without quotes");
				String newInputStr2 = inKeyboard.next();
				String[] attributes2 = newInputStr2.split(",");
				reader2.insert(attributes2);
				break;
			case SEVEN:
				this.writer1.writeEntries(this.reader1.getRecords());
				this.writer2.writeEntries(this.reader2.getRecords());
				break;
			case EIGHT:				
				inKeyboard.close();
				System.out.println("Thanks for using our system. Happy Day.");
				throw new GracefulTerminationException("Gracefully terminated");
			default:
				System.out.println("Incorrect option: " + choice +
						", please choose other option from the list below:\n\n");
				break;
			}
		} while (true);
	}
	
}
