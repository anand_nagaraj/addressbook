package com.anand.srccode.application;

/**
 * Class Options, User is presented with the list of options to choose from
 * 
 * @author anandnagaraj
 *
 */
public class Options
{
	private String book1;
	private String book2;
	
	public Options(String b1, String b2)
	{
		this.book1 = b1;
		this.book2 = b2;
	}
	
	
	/**
	 * Display the available option to user
	 */
	public void displayTheOptions()
	{		
		System.out.println("Welcome to Address Book Listing....");
		System.out.println("Here are your options to pick from\n");
		System.out.println("1: List Contents of Phone Book : " + this.book1);
		System.out.println("2: List Contents of Phone Book : " + this.book2);
		System.out.println("3: List Contents of all Phone Books");
		System.out.println("4: Display Unique complement from each book");
		System.out.println("5: Add entries to Book1: " + this.book1);
		System.out.println("6: Add entries to Book2: " + this.book2);
		System.out.println("7: Commit and Dump data to physical disk");
		System.out.println("8: Quit");
		System.out.print("Your Answer : ");
	}
}
