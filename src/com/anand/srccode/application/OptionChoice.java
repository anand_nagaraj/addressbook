package com.anand.srccode.application;

import java.util.HashMap;

/**
 * Enum, to extract and handle all input possible options.
 * 
 * @author anandnagaraj
 *
 */
public enum OptionChoice
{
	ONE(1),
	TWO(2),
	THREE(3),
	FOUR(4),
	FIVE(5),
	SIX(6),
	SEVEN(7),
	EIGHT(8),
	INFINITE(-1); // An option which is not valid
	

    private int value;
    private static HashMap<Integer, OptionChoice> map = new HashMap<>();

    private OptionChoice(int value)
    {
        this.value = value;
    }

    static
    {
        for (OptionChoice optionChoice : OptionChoice.values()) {
            map.put(optionChoice.value, optionChoice);
        }
    }

    public static OptionChoice valueOf(int choice)
    {
    	
        return (OptionChoice) map.get(choice) == null ? INFINITE : map.get(choice);
    }

    public int getValue() {
        return value;
    }
}
