package com.anand.srccode.writer;

import java.io.FileWriter;
import java.io.IOException;
import java.util.SortedMap;

/**
 * Class BookWriter, Used for writing the contents to the file.
 * Currently it takes only data entries from Map.
 * 
 * @author anandnagaraj
 *
 */
public class BookWriter implements WriterInterface
{
	private String m_name;
	
	public BookWriter(String name)
	{
		this.m_name = name;
	}
	
	
	/**
	 * Takes the sortedMap as its data structure and dumps them in 
	 * @param map
	 * @throws IOException
	 */
	@Override
	public void writeEntries(Object obj) throws IOException
	{
		@SuppressWarnings("unchecked")
		SortedMap<String, Integer> map = (SortedMap<String, Integer>) obj;
		try {
			FileWriter bookWriter = new FileWriter(this.m_name);
			for (String name : map.keySet()) {
				Integer phone = (Integer)map.get(name);
				bookWriter.write(name + "," + phone + "\n");		    
			}
			bookWriter.close();
		} catch (IOException ioe) {
			System.out.println("Caught IOException while writing a file to " + m_name
					+ " more details check the trace");
			throw ioe;
		}
	}
}
