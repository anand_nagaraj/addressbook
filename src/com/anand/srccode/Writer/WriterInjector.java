package com.anand.srccode.writer;


public interface WriterInjector {
	
	public WriterInterface getConsumerWriter(Object obj);

}
