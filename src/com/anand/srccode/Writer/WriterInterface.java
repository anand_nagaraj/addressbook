package com.anand.srccode.writer;

import java.io.IOException;


public interface WriterInterface
{
	/**
	 * Write entries back to Disk / DB 
	 * @param map
	 * @throws IOException
	 */
	public void writeEntries(Object obj) throws IOException;
}
