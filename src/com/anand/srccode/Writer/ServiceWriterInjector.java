package com.anand.srccode.writer;

public class ServiceWriterInjector implements WriterInjector {

	@Override
	public WriterInterface getConsumerWriter(Object obj)
	{
		String book = (String) obj;
		BookWriter writer = new BookWriter(book);
		return writer;
	}

}
