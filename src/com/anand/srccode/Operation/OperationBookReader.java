package com.anand.srccode.operation;

import java.util.SortedSet;
import java.util.TreeSet;


/**
 * Class OperationBookReader, Provides the required operation necessary for
 * operating the entries on each file.
 * 
 * The main functionality is to fetching the unique keys in two sets available.
 * 
 * @author anandnagaraj
 *
 */
public class OperationBookReader implements OperationInterface
{
	String leftAlignFormat = "| %-3d | %-20s |%n";
	
	private void prefixFormattedDisplay()
	{
		System.out.format("|--------- Unique Names -----|%n%n");
		System.out.format("+-----+----------------------+%n");
		System.out.format("| Num |      Name            |%n");
		System.out.format("+-----+----------------------+%n");
	}
	
	private void suffixFormattedDisplay()
	{
		System.out.format("+-----+----------------------+%n");
	}
	
	private SortedSet<String> getTheUnionKeys(SortedSet<String> s1, SortedSet<String> s2)
	{
		SortedSet<String> unionKeys = new TreeSet<>(s1);
		unionKeys.addAll(s2);
		
		return unionKeys;
	}
	
	private SortedSet<String> getAllButComplement(SortedSet<String> s1, SortedSet<String> s2, Boolean set1)
	{	
		SortedSet<String> unionKeys = getTheUnionKeys(s1, s2);
		if (set1) {
			unionKeys.removeAll(s1);
		} else {
			unionKeys.removeAll(s2);
		}
		return unionKeys;
	}
	
	/**
	 * Gather unique set of keys, Complement of INTERSECTION of sets
	 * @param s1
	 * @param s2
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Object gatherUniqueKey(Object obj1, Object obj2)
	{
		if (obj1 instanceof SortedSet<?> && obj2 instanceof SortedSet<?>) {
			SortedSet<String> s1 = (SortedSet<String>) obj1;
			SortedSet<String> s2 = (SortedSet<String>) obj2;

			SortedSet<String> tempUnionKeys_1 = getAllButComplement(s1, s2, true);
			SortedSet<String> tempUnionKeys_2 = getAllButComplement(s1, s2, false);

			SortedSet<String> unionKeys = new TreeSet<String>();
			unionKeys.addAll(tempUnionKeys_1);
			unionKeys.addAll(tempUnionKeys_2);

			return unionKeys;
		}
		return null;
	}
	
	/**
	 * Display the unique set of keys, A complement of INTERSECTION of sets
	 * @param obj1
	 * @param obj2
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void displayUniqueKeys(Object obj1, Object obj2)
	{
		if (obj1 instanceof SortedSet<?> && obj2 instanceof SortedSet<?>) {
			
			SortedSet<String> unionKeys = (SortedSet<String>) gatherUniqueKey(obj1, obj2);
			prefixFormattedDisplay();
			int count = 1;
			for (String name : unionKeys) {
				System.out.printf(leftAlignFormat, count++, name);
			}
			suffixFormattedDisplay();
		}
	}
}
