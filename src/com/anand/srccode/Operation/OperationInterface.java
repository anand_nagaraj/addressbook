package com.anand.srccode.operation;


/**
 * 
 * 
 * @author anandnagaraj
 *
 */
public interface OperationInterface
{
	/**
	 * Gather unique set of keys, Complement of INTERSECTION of sets
	 * @param s1
	 * @param s2
	 * @return
	 */
	public Object gatherUniqueKey(Object obj1, Object obj2);
	
	/**
	 * Display the unique set of keys, A complement of INTERSECTION of sets
	 * @param obj1
	 * @param obj2
	 */
	public void displayUniqueKeys(Object obj1, Object obj2);
}
